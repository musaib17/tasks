function Animal(Name, sound, genus) {
    this.Name = Name;
    this.sound = sound;
    this.genus = genus;
}
Animal.prototype.walk = function () {
    console.log("Normal walk");
}
Animal.prototype.talk = function () {
    console.log(`Sound: ${this.sound}`);
}
Animal.prototype.toString = function () {
    console.log(`Genus name: ${this.genus}`);
};

dog.prototype = Object.create(Animal.prototype);
function dog(Name, sound, genus) {
    Animal.call(this, Name, sound, genus)
}
dog.prototype.walk = function () {
    console.log("Dog's walk");
}

cat.prototype = Object.create(Animal.prototype);
function cat(Name, sound, genus) {
    Animal.call(this, Name, sound, genus)
}
cat.prototype.walk = function () {
    console.log("Cat's walk");
}

lion.prototype = Object.create(Animal.prototype);
function lion(Name, sound, genus) {
    Animal.call(this, Name, sound, genus)
}

let tommy = new dog("Tommy", "barks", "huskey")
console.log(`Tommy is dog`);
tommy.walk();
tommy.talk();
tommy.toString();

console.log();
let sophie = new cat("Sophie", "meows", "persian")
console.log(`Sophie is cat`);
sophie.walk();
sophie.talk();
sophie.toString();

console.log();
let shera = new lion("Shera", "roars", "panthera")
console.log(`Shera is lion`);
shera.walk();
shera.talk();
shera.toString();

function Birds(Name, chrip, genus) {
    this.Name = Name;
    this.chrip = chrip;
    this.genus = genus;
}
Birds.prototype.chirp = function () {
    console.log(`Sound name: ${this.chrip}`);
}
Birds.prototype.fly = function () {
    console.log("Genric fly");
}
Birds.prototype.toString = function () {
    console.log(`genus name : ${this.genus}`);
}

kingfisher.prototype = Object.create(Birds.prototype);
function kingfisher(Name, chirp, genus) {
    Birds.call(this, Name, chirp, genus)
}
kingfisher.prototype.fly = function () {
    console.log("kingfisher's fly");
}

quail.prototype = Object.create(Birds.prototype);
function quail(Name, chirp, genus) {
    Birds.call(this, Name, chirp, genus)
}
quail.prototype.fly = function () {
    console.log("quail's fly");
}

crow.prototype = Object.create(Birds.prototype);
function crow(Name, chirp, genus) {
    Birds.call(this, Name, chirp, genus)
}

console.log();
let bubba = new kingfisher("Bubba", "strident rattles", "Halcyon")
console.log("Bubba is kingfisher");
bubba.fly();
bubba.chirp();
bubba.toString();

console.log();
let zippy = new quail("Zippy", "Chicago", "common quail")
console.log("Zippy is quail");
zippy.fly();
zippy.chirp();
zippy.toString();

console.log();
let plucky = new crow("Plucky", "caw", "corvus")
console.log("Plucky is crow");
plucky.fly();
plucky.chirp();
plucky.toString();